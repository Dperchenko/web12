$(document).on("ready", function() {
  $(".regular").slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            dots: true
          }
        }
    ]
  });

  $(".ajaxForm").submit(function(e){
      e.preventDefault();
      var href = $(this).attr("action");
      $.ajax({
        type: "POST",
        dataType: "json",
        url: href,
        data: $(this).serialize(),
        success: function(response){
        if(response.status == "success"){
          alert("We received your submission, thank you!");
        }else{
          alert("An error occured: " + response.message);
        }
      }
    });
  });

});

function clickButtom(){
  document.getElementById("InputName1").value="";
  document.getElementById("InputPhoneNumber1").value="";
  selectElement('region', 'default')
  document.getElementById("textareaNoResize").value="";
}

function fioInput(e){
  console.log(e.target.value)
  localStorage.setItem("FIO", e.target.value)
}

function phoneInput(e){
  console.log(e.target.value)
  localStorage.setItem("number", e.target.value)
}

function regionInput(e){
  console.log(e.target.value)
  localStorage.setItem("region", e.target.value)
}

function messageInput(e){
  console.log(e.target.value)
  localStorage.setItem("message", e.target.value)
}


function setFio(){
  if (localStorage.getItem("FIO") !== null){
    document.getElementById("InputName1").value=localStorage.getItem("FIO")
  }
}

function setPhone(){
  if (localStorage.getItem("number") !== null){
    document.getElementById("InputPhoneNumber1").value =localStorage.getItem("number")
  }
}

function setRegion(){
  if (localStorage.getItem("region") !== null){
    document.getElementById("region").value=localStorage.getItem("region")
  }
}

function setMessage(){
  if (localStorage.getItem("message") !== null){
    document.getElementById("textareaNoResize").value=localStorage.getItem("message")
  }
}


function selectElement(id, valueToSelect) {    
  let element = document.getElementById(id);
  element.value = valueToSelect;
}

setFio();
setPhone();
setRegion();
setMessage();

  window.onclick = function(event) {
  if (event.target == exampleModalCenter) {
    $('#exampleModalCenter').hide();
    // myTest();
    }
    $("#Fader2").removeClass("fadeout").addClass("fadein");
    $("#Fader1").removeClass("fadeout1").addClass("fadein1");
  }

  $("#Trigger2").click(function () {
    if ($("#Fader2").hasClass("fadeout"))
        $("#Fader2").removeClass("fadeout").addClass("fadein");
    else
        $("#Fader2").removeClass("fadein").addClass("fadeout");
  });
  $("#Trigger1").click(function () {
    if ($("#Fader1").hasClass("fadeout1"))
        $("#Fader1").removeClass("fadeout1").addClass("fadein1");
    else
        $("#Fader1").removeClass("fadein1").addClass("fadeout1");
  });

  addEventListener("popstate",function(){
      history.pushState({page: 1}, "title 1", "?page=1");
      $('#exampleModalCenter').hide();
      $("body").attr("style", "overflow: unset");
  }, true);

function myButton(){
  jQuery.noConflict(); 
  // Создаем элемент для анимации.
  var el = document.getElementById("exampleModalCenter");
  el.style.opacity = "0";
  $('#exampleModalCenter').modal('toggle');
  el.style.opacity = 0;
  var start = Date.now();
  var tick = function() {
    el.style.opacity = (Date.now() - start) / 400;
    if (el.style.opacity < 1) {
      (window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
    }
  };
  tick();
}

function myTest(){
  jQuery.noConflict(); 
  // Создаем элемент для анимации.
  var el = document.getElementById("exampleModalCenter");
  el.style.opacity = "1";
  $('#exampleModalCenter').modal('toggle');
  el.style.opacity = 1;
  var start = Date.now();
  var tick1 = function() {
    el.style.opacity = (1 -((Date.now() - start) / 400));
    if (el.style.opacity > 0) {
      (window.requestAnimationFrame && requestAnimationFrame(tick1)) || setTimeout(tick1, 16);
    }
  };
  tick1();
}